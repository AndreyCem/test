import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        int add = calculator.add(5, 5);
        Assert.assertEquals(10, add);
    }

    @Test
    public void testSub() {
        Calculator calculator = new Calculator();
        int sub = calculator.sub(5, 5);
        Assert.assertEquals(0, sub);
    }

    @Test
    public void testMil() {
        Calculator calculator = new Calculator();
        int mil = calculator.mil(5, 3);
        Assert.assertEquals(15, mil);
    }
}